
from django.http import HttpResponse
from django.template.loader import get_template
from weasyprint import HTML, CSS
from django.template import TemplateDoesNotExist
import requests
from datetime import date
import os


def printPDF(template_src, css_url, context_dict=None):
    if context_dict is None:
        context_dict = {}
        
    try:
        template = get_template(template_src)
        html_template = template.render(context_dict)
        
        # Intenta generar el PDF
        pdf_file = HTML(string=html_template).write_pdf(stylesheets=[CSS(css_url)])
        
        # Si se genera correctamente, devuelve el PDF como una respuesta HTTP
        return HttpResponse(pdf_file, content_type='application/pdf')
        
    except TemplateDoesNotExist as e:
        # Maneja el caso en que el template no existe
        error_message = f"Error: El template '{template_src}' no existe."
        return HttpResponse(error_message, status=500)
        
    except Exception as e:
        # Maneja cualquier otro error que pueda ocurrir durante el proceso
        error_message = f"Error: {str(e)}"
        return HttpResponse(error_message, status=500)


def daily_landscape():
    unsplash_access_key = 'EQkJG0PIj4iw5vmaZNTgSxAnMaPmQJ_VXGPYeXluGTE'

    #Realizar la solicitud
    response = requests.get('https://api.unsplash.com/photos/random', params={
            'query': 'landscape',
            'orientation': 'landscape',
            'client_id': unsplash_access_key
    })

    #obtener url de la imagen
    if response.status_code == 200:
        data = response.json()
        image_url = data['urls']['regular']
    else:
        # Si la solicitud falla, puedes proporcionar una imagen de respaldo
        image_url =  os.path.join(settings.BASE_DIR, 'orders/static/img/PPlogo.jpg')

    return image_url


def doOrder(form, Client, Order):
    clientee = form.cleaned_data['client']
    namecliente = form.cleaned_data['nameclient']
    numbercliente = form.cleaned_data['numberclient']  
    descripcionn = form.cleaned_data['description'] 
    deadlinee = form.cleaned_data['deadline']
    responsiblee = form.cleaned_data['responsible'] 
    anticipoo = form.cleaned_data['anticipo']
    totall = form.cleaned_data['total']  

    cliente_existente = Client.objects.filter(phone=numbercliente).exists()

    if not cliente_existente:
                nuevo_cliente = Client (
                   name = namecliente,
                   phone = numbercliente 
                )
                nuevo_cliente.save()
                clientee = nuevo_cliente.id
    else:
                aux =  Client.objects.get(phone=numbercliente)
                clientee = aux.id
            
    if anticipoo == totall:
                entregadoo = True
    else:
                entregadoo = False
            
            
    pedido = Order(
                client_id=clientee,
                description = descripcionn,
                request_date = date.today(),
                deadline = deadlinee,
                responsible_id=responsiblee,
                anticipo=anticipoo,
                entregado=entregadoo,
                total=totall
                )
                

    pedido.save()
     
    return pedido.id