from django import forms
from .models import Order

class customOrderForm(forms.Form):
    client = forms.IntegerField(label='clienteid',required = False,widget=forms.NumberInput(
        attrs={
            'id':'cclienteid'
        }
    ))
    nameclient = forms.CharField(label='name', widget=forms.TextInput(
        attrs={
        'id':'nnameclient'
        }))
    numberclient = forms.IntegerField(label='numero',widget=forms.NumberInput(
        attrs={
            'id':'nnumberclient'
        }
    ))
    description = forms.CharField(label='descripcion', widget=forms.TextInput(
        attrs={
        'id':'rresumen'
        }))
    deadline = forms.DateField(label='Entrega', widget=forms.DateInput(
        attrs={
            'id':'eentrega'
        }
    ))
    responsible = forms.IntegerField(label='responsableid', widget=forms.NumberInput(
        attrs={
            'id':'rresponsableid'
        }
    ))
    anticipo = forms.IntegerField(label='anticipo', widget=forms.NumberInput(
        attrs={
            'id':'aanticipo'
        }
    ))
    total=forms.IntegerField(label='total', widget=forms.NumberInput(
        attrs={
            'id':'ttotal'
        }
    ))

class OrderModelForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['client', 'request_date']
        widgets = {
            'request_date': forms.DateInput(attrs={'type': 'date'}),
            'client' : forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'request_date' : 'Pedido',
            'client': 'Cliente',
        }