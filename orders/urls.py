from . import views
from django.urls import path


urlpatterns = [
    path('', views.OrderListView.as_view(), name='proforma'),
    path('crear/', views.ClientOrderListView.as_view(), name='orden'),
    path('i/<uuid:uuid>/', views.OrderPrint.as_view(), name='imprimirOrden'),
    path('ver/<int:pk>/', views.DetailOrderView.as_view(), name='verOrden'),
    path('editar/<int:pk>/', views.OrderEditeView.as_view(), name='pedido_editar'),
    path('eliminar/<int:pk>/', views.OrderDeleteView.as_view(), name='pedido_delete'),
    path('entregar/<int:pedido_id>/', views.entregar_pedido, name='entregar_pedido'),
]

