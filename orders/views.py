from django.contrib.auth.mixins import LoginRequiredMixin
import os
from django.shortcuts import redirect, render
from django.views.generic import ListView, CreateView, DeleteView, View, DetailView
from django.db.models import Q
from publiprint import settings
from .models import Order, Employee
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import TemplateView, UpdateView
from django.shortcuts import get_object_or_404
from .forms import customOrderForm, OrderModelForm
from datetime import date
from .utils import printPDF, daily_landscape, doOrder
from django.utils.encoding import smart_str
from django.contrib.staticfiles import finders
from django import forms
from clientes.models import Client
import json


class OrderEditeView(LoginRequiredMixin, UpdateView):
    model = Order
    template_name = 'pedido/editOrder.html'
    fields = ['id', 'client', 'description', 'request_date', 'deadline', 'anticipo', 'total']
    
    def get_form(self, form_class=None):
        form = super(OrderEditeView, self).get_form(form_class)
        form.fields['request_date'].widget = forms.DateInput(attrs={'type': 'date'})
        form.fields['deadline'].widget = forms.DateInput(attrs={'type': 'date'})
        return form

    def get_success_url(self):
        return reverse('verOrden', kwargs={'pk': self.object.id})

class OrderDeleteView(LoginRequiredMixin, DeleteView):
    model = Order
    template_name = 'pedido/confirm_delete.html'
    success_url = reverse_lazy('proforma')

def entregar_pedido(request, pedido_id):
    pedido = get_object_or_404(Order, pk=pedido_id)
    pedido.entregar()  # Llamando al método entregar del modelo
 
    # return redirect('cajas:caja_list')
    return HttpResponseRedirect(reverse('verOrden', kwargs={'pk': pedido_id}))

class OrderListView(LoginRequiredMixin, ListView):
    paginate_by = 10
    model = Order
    template_name = 'pedido/listOrder.html'
    context_object_name = 'pedidos'

    def get_queryset(self):
        queryset = Order.objects.all().order_by('-created')
        query_article = self.request.GET.get('ord_article')
        query_name = self.request.GET.get('ord_name')
        query_phone = self.request.GET.get('ord_phone')

        filters = Q()
        if query_article:
            filters &= Q(description__icontains=query_article)
        if query_name:
            filters &= Q(client__name__icontains=query_name)
        if query_phone:
            normalized_phone = query_phone.replace(" ", "")
            filters &= Q(client__phone__icontains=normalized_phone)

        return queryset.filter(filters)
        


#PARA Crear una ORDERS
class ClientOrderListView(LoginRequiredMixin, ListView):
    model = Client
    template_name = 'pedido/createOrder.html'
    context_object_name = 'clientes'
    paginate_by = 10 

    def post(self, request, *args, **kwargs):
        form = customOrderForm(request.POST)
        if form.is_valid():
            newPedidoID = doOrder(form, Client, Order)
            return redirect(reverse('verOrden', kwargs={'pk': newPedidoID}))
        return redirect('orden')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = customOrderForm()
        context['empleados'] = Employee.objects.filter(is_working=True, position='atencion')
        return context



class OrderPrint(LoginRequiredMixin, View):
    def get(self, request, uuid, *args, **kwargs):
        orden = Order.objects.get(uuid=uuid)
        total = orden.total
        anticipo = orden.anticipo
        resta = total - anticipo
        css_url = os.path.join(settings.BASE_DIR, 'static/css/proforma.css')
        img_url = finders.find('PPlogo.jpeg')
        data = {
            'pedido': orden,
            'saldoo': resta,
            'logo':img_url,
        }
        pdf = printPDF('pedido/printOrder.html', css_url, data)
        response = HttpResponse(pdf, content_type='application/pdf')
        nombre_archivo = "Mipedido.pdf"
        response['Content-Disposition'] = 'inline; filename*=UTF-8\'\'{}'.format(smart_str(nombre_archivo))
        
        # response['Content-Disposition'] = 'attachment; filename="pedido.pdf"'
        return response

class DetailOrderView(LoginRequiredMixin, DetailView):
    model = Order
    template_name = 'pedido/viewOrder.html'  # Nombre de la plantilla a utilizar
    context_object_name = 'pedido' 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        orden = self.object
        saldo = orden.total - orden.anticipo
        context['saldo'] = saldo
        return context

    
