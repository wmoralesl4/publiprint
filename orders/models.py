from django.db import models
from django.core.exceptions import ValidationError
from clientes.models import Client
from caja.models import TipoCaja
from empleados.models import Employee
import uuid

def validate_eight_digits(value):
    if len(str(value)) != 8:
        raise ValidationError('El valor debe tener exactamente 8 dígitos.')



class Article(models.Model):
    name=models.CharField(max_length=60)
    created=models.DateField(auto_now_add=True)
    updated=models.DateField(auto_now=True)
    class Meta:
        verbose_name="Producto"
        verbose_name_plural="Productos"

    def __str__(self):
        return self.name
    
class Order(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    description = models.TextField()
    request_date = models.DateField()
    deadline = models.DateField()
    responsible = models.ForeignKey(Employee, on_delete=models.CASCADE)
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateField(auto_now=True)
    anticipo = models.IntegerField()
    entregado = models.BooleanField()
    total = models.IntegerField()
    caja = models.ForeignKey(TipoCaja,on_delete=models.CASCADE, default=1)
    class Meta:
        verbose_name="Pedido"
        verbose_name_plural="Pedidos"

    def getSaldo(self):
        saldo = self.total - self.anticipo
        return saldo
    
    def entregar(self):
        if self.entregado:
            self.entregado = False
        else:
            self.entregado = True
        self.save()

    def totalentrada(self):
        if self.entregado:
            return self.total
        else:
            return self.anticipo


