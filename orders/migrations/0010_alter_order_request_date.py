# Generated by Django 5.0.3 on 2024-03-26 22:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_order_anticipo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='request_date',
            field=models.DateField(),
        ),
    ]
