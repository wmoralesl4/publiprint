from django.contrib import admin
from .models import *

# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    readonly_fields=('created', 'updated')
    list_display = ('id','name', 'phone', 'created', 'updated')

class EmployeeAdmin(admin.ModelAdmin):
    readonly_fields=('created', 'updated')
    list_display = ('id','name', 'phone', 'created', 'updated')

class ArticleAdmin(admin.ModelAdmin):
    readonly_fields=("created", "updated")
    list_display = ('name', 'created', 'updated')

class OrderAdmin(admin.ModelAdmin):
    readonly_fields=("created", "updated")
    list_display = ('id','client', 'responsible', 'request_date', 'deadline','anticipo', 'total', 'entregado')

admin.site.register(Client, ClientAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Order, OrderAdmin)