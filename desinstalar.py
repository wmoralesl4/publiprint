import subprocess

# Lista de paquetes a conservar
paquetes_conservar = [
    "asgiref==3.7.2",
    "Django==5.0.3",
    "djangorestframework==3.14.0",
    "pytz==2024.1",
    "sqlparse==0.4.4",
    "tzdata==2024.1"
]

# Obtener la lista de paquetes instalados
salida = subprocess.check_output(["pip", "freeze"]).decode("utf-8")
paquetes_instalados = [linea.strip() for linea in salida.split("\n")]

# Determinar los paquetes a desinstalar
paquetes_a_desinstalar = []
for paquete in paquetes_instalados:
    if paquete not in paquetes_conservar:
        paquetes_a_desinstalar.append(paquete)

# Desinstalar los paquetes
for paquete in paquetes_a_desinstalar:
    subprocess.call(["pip", "uninstall", "-y", paquete])
    print(f"Desinstalando {paquete}")

print("Proceso completado.")