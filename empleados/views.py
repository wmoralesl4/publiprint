from django.shortcuts import render
from orders.models import Employee
from django.views.generic import ListView, CreateView, DeleteView, View, DetailView, UpdateView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
#############################################


class EmployeeListView(LoginRequiredMixin, ListView):
    model = Employee
    template_name = 'empleado/listEmployee.html'
    context_object_name = 'empleados'
    paginate_by = 10

    def get_queryset(self):
        query = self.request.GET.get('query_empleados')
        if query:
            return Employee.objects.filter(name__icontains=query).order_by('id')
        return Employee.objects.all().order_by('id')

class EmployeeCreateView(LoginRequiredMixin, CreateView):
    model = Employee
    template_name = 'empleado/createEmployee.html'
    fields = ['name', 'phone']
    success_url = reverse_lazy('list_employee')

class EmployeeDetailView(LoginRequiredMixin, DetailView):
    model = Employee
    template_name = 'empleado/detailEmployee.html'
    context_object_name = 'empleado'

class EmployeeUpdateView(LoginRequiredMixin, UpdateView):
    model = Employee
    template_name = 'empleado/updateEmployee.html'
    fields = ['name', 'phone', 'is_working']
    success_url = reverse_lazy('list_employee')

class EmployeeDeleteView(LoginRequiredMixin, DeleteView):
    model = Employee
    template_name = 'empleado/deleteEmployee.html'
    success_url = reverse_lazy('list_employee')