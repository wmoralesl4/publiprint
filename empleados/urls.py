from . import views
from django.urls import path


urlpatterns = [
    path('', views.EmployeeListView.as_view(), name='list_employee'),
    path('crear/', views.EmployeeCreateView.as_view(), name='create_employee'),
    path('<int:pk>/', views.EmployeeDetailView.as_view(), name='detail_employee'),
    path('actualizar/<int:pk>/', views.EmployeeUpdateView.as_view(), name='update_employee'),
    path('eliminar/<int:pk>/', views.EmployeeDeleteView.as_view(), name='delete_employee'),
]
