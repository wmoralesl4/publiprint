from . import views
from django.urls import path


urlpatterns = [
    path('', views.home.as_view(), name='home'),
    path('calendario/', views.calendarView.as_view(), name='calendar'),

]
