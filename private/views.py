from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, UpdateView
from clientes.models import Client
from datetime import date, timedelta
from empleados.models import Payment
from orders.models import Order
from .graphs import lastTwelveMonths, lastTwelveMonthsName
import json
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Q
import calendar

# Create your views here.
class home(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('auth:login')
    template_name = 'utilidades/inicio.html'

    def get_context_data(self, **kwargs):
        Current_date = date.today()
        Current_month = Current_date.month
        Current_year = Current_date.year
        context = super().get_context_data(**kwargs)
        all_clients = Client.objects.count()
        all_orders = Order.objects.count()

        orders_this_month = Order.objects.filter(Q(request_date__month=Current_month)).count()
        clients_this_month = Client.objects.filter(Q(created__month=Current_month)).count()

        orders_this_day = Order.objects.filter(Q(request_date=Current_date))
        delivery_now = Order.objects.filter(deadline=Current_date)        # Datos para los gráficos
        delivery_tomorrow = Order.objects.filter(deadline=Current_date+timedelta(days=1))

        clients_data = lastTwelveMonths(Client, "clientes", Current_date)
        orders_data = lastTwelveMonths(Order, "orders", Current_date)
        months_name = lastTwelveMonthsName(Current_date)

        context['current_date'] = Current_date
        context['allcients'] = all_clients
        context['allorders'] = all_orders
        context['ordersthismonth'] = orders_this_month
        context['clienthismonth'] = clients_this_month
        context['orders_this_day'] = orders_this_day
        context['delivery_now'] = delivery_now
        context['delivery_tomorrow'] = delivery_tomorrow
        context['clients_data'] = json.dumps(clients_data)
        context['orders_data'] = json.dumps(orders_data)
        context['months_name'] = json.dumps(months_name)
        return context

class calendarView(LoginRequiredMixin, TemplateView):
    template_name = 'utilidades/calendar.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        orders = Order.objects.all()

        # Crear eventos para órdenes
        events = [
            {
            'title': order.client.name,
            'start': order.deadline.strftime('%Y-%m-%d'),
            'end': order.deadline.strftime('%Y-%m-%d'),
            'url': reverse('verOrden', kwargs={'pk': order.id}),
            'color': 'green' if order.entregado else 'red',
            } for order in orders
        ]

        # Obtener el mes y año actual
        today = date.today()
        year = today.year
        month = today.month

        # Obtener todos los pagos
        payments = Payment.objects.filter(Q(employee__is_working=True))
        # payments = Payment.objects.all()
        # Crear eventos para pagos
        payment_events = []
        for payment in payments:
            # Validar que el día sea válido en el mes actual
            if 1 <= payment.date <= calendar.monthrange(year, month)[1]:
                payment_events.append({
                    'title': f'• {payment.employee.name}',
                    'start': f'{year}-{month:02d}-{payment.date:02d}',
                    'end': f'{year}-{month:02d}-{payment.date:02d}',
                    'color': 'blue',  # Color distintivo para pagos
                })

    # Combinar los eventos de órdenes y pagos
        events.extend(payment_events)

        context = {
        'events': json.dumps(events)
        }
        return context