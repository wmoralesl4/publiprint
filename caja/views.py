from django.utils import timezone

from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from caja.models import Salida
from orders.models import Order
from django.db.models import Q


def get_combined_list():
    entradas = Order.objects.all()
    salidas = Salida.objects.all()
    
    # Combinar ambas consultas en una lista
    lista_combinada = list(entradas) + list(salidas)
    
    # Ordenar la lista combinada por fecha
    lista_ordenada = sorted(lista_combinada, key=lambda obj: obj.created)
    
    return lista_ordenada

def totales(lista_ord):
    totals = {}  # Diccionario para almacenar los totales
    
    for movimiento in lista_ord:
        if movimiento.caja.id == 1:
            if 'total_entrada' not in totals:
                totals['total_entrada'] = 0
            totals['total_entrada'] += movimiento.totalentrada()
        else:
            if 'total_salida' not in totals:
                totals['total_salida'] = 0
            totals['total_salida'] += movimiento.total 
            
    return totals

class CajaListView(ListView):
    model = Salida
    context_object_name = 'salidas'
    template_name = 'caja/list.html'

    def get_queryset(self):
        query = self.request.GET.get('caja_date')

        lista_combinada = get_combined_list()
        if query:
            year, month = query.split("-")
            year, month = int(year), int(month)
   
            return [item for item in lista_combinada if item.created.year == year and item.created.month == month]
        else:
            year, month = timezone.now().year, timezone.now().month
            return [item for item in lista_combinada if item.created.year == year and item.created.month == month]
                      
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lista_combinada = self.get_queryset()
        
        
        # Obtener los totales
        totals = totales(lista_combinada)
        alltotals = totals.get('total_entrada', 0) - totals.get('total_salida', 0)

        context['alltotals'] = alltotals
        context['total'] = totals
        context['movimientos'] = lista_combinada
        return context

class SalidaListView(ListView):
    model = Salida
    context_object_name = 'salidas'
    paginate_by = 10
    template_name = 'salida/list.html'
    order_by = '-created'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.order_by(self.order_by)

class CajaDetailView(DetailView):
    model = Salida
    template_name = 'caja/detail.html'

class CajaCreateView(CreateView):
    model = Salida
    template_name = 'salida/form.html' 
    fields = ['description', 'total']
    success_url = reverse_lazy('cajas:salida_list')

class CajaUpdateView(UpdateView):
    model = Salida
    template_name = 'salida/editar.html' 
    fields = ['description', 'total']
    success_url = reverse_lazy('cajas:salida_list')

class CajaDeleteView(DeleteView):
    model = Salida
    template_name = 'caja/confirm_delete.html'
    success_url = reverse_lazy('cajas:salida_list')