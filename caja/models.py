from django.db import models

class TipoCaja(models.Model):
    nombre = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre
    
# Create your models here.

class Salida(models.Model):
    caja = models.ForeignKey(TipoCaja,on_delete=models.CASCADE, default=2)
    description = models.TextField()
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateField(auto_now=True)
    total = models.FloatField()
    def __str__(self):
        return self.description
    