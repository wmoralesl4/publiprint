from django.urls import path
from . import views

app_name = 'cajas'

urlpatterns = [
    path('caja/', views.CajaListView.as_view(), name='caja_list'),
    path('salidas/', views.SalidaListView.as_view(), name='salida_list'),
    path('<int:pk>/', views.CajaDetailView.as_view(), name='caja_detail'),
    path('create/', views.CajaCreateView.as_view(), name='caja_create'),
    path('edit/<int:pk>/', views.CajaUpdateView.as_view(), name='salida_edit'),
    path('delete/<int:pk>', views.CajaDeleteView.as_view(), name='caja_delete'),


]
