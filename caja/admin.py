from django.contrib import admin
from caja.models import TipoCaja, Salida
# Register your models here.

admin.site.register(TipoCaja)
admin.site.register(Salida)