
$(function() {
    $(".autocomplete").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: clientSuggestionsUrl,
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.name + " (" + item.phone + ")",
                            value: item.name,
                            phone: item.phone,
                            id: item.id
                        };
                    }));
                }
            });
        },
        select: function(event, ui) {
            $("#nombrepedido").val(ui.item.value);
            $("#numeropedido").val(ui.item.phone);
            $("#idclientepedido").val(ui.item.id);
            return false;
        }
    });
});