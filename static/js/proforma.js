
    function copiarValores(){
        try {
            var pedidoidcliente = document.getElementById('idclientepedido').value
            document.getElementById('cclienteid').value = parseInt(pedidoidcliente)
        } catch (error) {
            
        }
        
        var pedidoNombre = document.getElementById('nombrepedido').value
        document.getElementById('nnombre').value = pedidoNombre
        document.getElementById('nnameclient').value = pedidoNombre

        var pedidoNumero = document.getElementById('numeropedido').value
        pedidoNumero = pedidoNumero.replace(/\s+/g, '')
        console.log(pedidoNumero)
        document.getElementById('nnumero').value = pedidoNumero
        document.getElementById('nnumberclient').value = pedidoNumero

        var pedidoFecha = document.getElementById('fechaentrega').value
        document.getElementById('eentrega').value = pedidoFecha
        document.getElementById('ffecha').value = pedidoFecha
        // rresponsableid
        var pedidoResponsable = document.getElementById('elresponsable')
        document.getElementById('rresponsableid').value = pedidoResponsable.value
        document.getElementById('rresponsable').value = pedidoResponsable.options[pedidoResponsable.selectedIndex].text
    }


    function agregarFila() {

        var table = document.getElementById("tablaProductos").getElementsByTagName('tbody')[0];
        var newRow = table.insertRow(table.rows.length);

        var cell1 = newRow.insertCell(0);
        var cell2 = newRow.insertCell(1);
        var cell3 = newRow.insertCell(2);
        var cell4 = newRow.insertCell(3);
        var cell5 = newRow.insertCell(4);

        cell1.innerHTML = '<input type="number" value="1" onclick="actualizarTotal()" onkeyup="actualizarTotal()">';
        cell2.innerHTML = '<textarea class="woutborder" placeholder="Escriba su producto aquí"></textarea>';
        cell3.innerHTML = '<input type="number" step="5" value="0" onclick="actualizarTotal()" onkeyup="actualizarTotal()">';
        cell4.innerHTML = '<input type="number" step="0.01" value="0.00" disabled>';
        cell5.innerHTML = '<i class="bi bi-x button red" onclick="eliminarFila(this.parentNode.parentNode)"></i>';

        // Actualizar el total y el resumen
        actualizarTotal();
        actualizarResumen();
    }

    function eliminarFila(row) {
        var rowIndex = row.rowIndex;
        document.getElementById("tablaProductos").deleteRow(rowIndex);
        actualizarTotal();
        actualizarResumen();
    }

    function actualizarTotal() {
        var table = document.getElementById("tablaProductos").getElementsByTagName('tbody')[0];
        var total = 0;
        var descripcion = "";

        for (var i = 0; i < table.rows.length; i++) {
            var cantidad = parseFloat(table.rows[i].cells[0].querySelector('input').value);
            var descripcionProducto = table.rows[i].cells[1].querySelector('textarea').value;
            var precioUnitario = parseFloat(table.rows[i].cells[2].querySelector('input').value);
            var subtotal = cantidad * precioUnitario;
            table.rows[i].cells[3].querySelector('input').value = subtotal.toFixed(2);
            total += subtotal;

            // Concatenar información para el resumen
            descripcion += " • "+ cantidad + " " + descripcionProducto + " - Subtotal: Q" + subtotal.toFixed(2) + "\n ";
            
        }

        document.getElementById('total').innerHTML = 'Q' + total.toFixed(2);

        return descripcion;
    }

    function actualizarResumen() {
        
        var descripcion = actualizarTotal();

        // Obtener el valor del anticipo
        var anticipo = parseFloat(document.getElementById('anticipo').value);
      

        // Calcular el total y el saldo
        var total = parseFloat(document.getElementById('total').innerHTML.slice(1)); // Q00.00
        var saldo = total - anticipo;

        // Llenar la tabla de resumen
        var resumenBody = document.getElementById("resumenBody");
        resumenBody.innerHTML = "";
        var newRow = resumenBody.insertRow();
        var cell1 = newRow.insertCell(0);
        var cell2 = newRow.insertCell(1);
        var cell3 = newRow.insertCell(2);
        var cell4 = newRow.insertCell(3);

        cell1.innerHTML =  descripcion;
        cell2.innerHTML = 'Q' + total.toFixed(2);
        cell3.innerHTML = 'Q' + anticipo.toFixed(2);
        cell4.innerHTML = 'Q' + saldo.toFixed(2);
        
        
        var resu = document.getElementById("rresumen")
        var anti = document.getElementById("aanticipo")
        var tota = document.getElementById("ttotal")
        
        resu.value = descripcion
        anti.value = anticipo.toFixed(2)
        tota.value = total.toFixed(2)
        
        copiarValores();
    }


document.getElementById('openPopup').addEventListener('click', function () {
    document.getElementById('popup').style.display = 'block';
});

document.getElementById('closePopup').addEventListener('click', function () {
    document.getElementById('popup').style.display = 'none';
});
