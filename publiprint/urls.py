"""
URL configuration for publiprint project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))no template
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("private.urls")),
    path('', include("authentication.urls")),
    path('pedidos/', include("orders.urls")),
    path('clientes/', include("clientes.urls")),
    path('empleados/', include("empleados.urls")),
    path('backup/', include ("db_backup.urls")),
    path('caja/', include("caja.urls")),
    path('api-auth/', include('rest_framework.urls')),

]
