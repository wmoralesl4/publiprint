from django.shortcuts import render
from .models import Client
from django.views.generic import ListView, CreateView, DeleteView, View, DetailView, UpdateView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from orders.models import Order
from django.http import JsonResponse
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class clientDetailView(LoginRequiredMixin, DetailView):
    template_name = 'cliente/viewClient.html'
    model = Client

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client = self.get_object()  # Obtener la instancia del cliente actual
        context['pedidos'] = Order.objects.filter(Q(client__id__iexact=client.id))
        return context
    
class ClientListView(LoginRequiredMixin, ListView):
    model = Client
    template_name = 'cliente/listClient.html'
    context_object_name = 'clientes'
    paginate_by = 10 
    def get_queryset(self):
        query = self.request.GET.get('query_clientes')
        if query:
            return Client.objects.filter(Q(name__icontains=query)).order_by('id')
        else:
            return Client.objects.all().order_by('-created')

class ClientCreatedView(LoginRequiredMixin, CreateView):
    model = Client
    template_name = 'cliente/createClient.html'
    success_url = reverse_lazy('client_list')
    fields = ['name', 'phone']
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('client_list'))
    

class ClientUpdateView(LoginRequiredMixin, UpdateView):
    model = Client
    template_name = 'cliente/updateClient.html'
    fields = ['name', 'phone']

    def form_valid(self, form):
        # messages.success(self.request, "Cliente actualizado con éxito.")
        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse('client_detail', kwargs={'pk': self.object.id})

class ClientDeleteView(LoginRequiredMixin, DeleteView):
    model = Client
    template_name = 'cliente/deleteClient.html'
    success_url = reverse_lazy('client_list')

    def delete(self, request, *args, **kwargs):
        # messages.success(self.request, "Cliente eliminado con éxito.")
        return super().delete(request, *args, **kwargs)

###############################################
    
class ClientSuggestionsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
            term = request.GET.get('term', '')
            clientes = Client.objects.filter(Q(name__icontains=term) | Q(phone__icontains=term))
            results = [{'id': cliente.id, 'name': cliente.name, 'phone': cliente.phone} for cliente in clientes]
            return JsonResponse(results, safe=False)
        return JsonResponse([], safe=False)
    

