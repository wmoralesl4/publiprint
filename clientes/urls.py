from . import views
from django.urls import path


urlpatterns = [
    path('', views.ClientListView.as_view(), name='client_list'),
    path('crear/', views.ClientCreatedView.as_view(), name='client_create'),
    path('<int:pk>/', views.clientDetailView.as_view(), name = 'client_detail'),
    path('actualizar/<int:pk>/', views.ClientUpdateView.as_view(), name='client_update'),
    path('eliminar/<int:pk>/', views.ClientDeleteView.as_view(), name='client_delete'),

    
    path('client-suggestions/', views.ClientSuggestionsView.as_view(), name='client-suggestions'),
]
