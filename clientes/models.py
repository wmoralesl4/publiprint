# clientes/models.py
from django.db import models
from django.core.exceptions import ValidationError

def validate_eight_digits(value):
    if len(str(value)) != 8:
        raise ValidationError('El valor debe tener exactamente 8 dígitos.')

class Client(models.Model):
    name = models.CharField(max_length=60)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)
    phone = models.IntegerField(
        validators=[validate_eight_digits],
        blank=True, null=True
    )

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __str__(self):
        return self.name
